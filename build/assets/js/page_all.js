/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2024. MIT licensed.
 */$(document).ready(function () {
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });
  $('.navbar-collapse .close,.navbar-nav a').on('click', function () {
    $('.navbar-collapse').removeClass('show');
    $('.navbar-toggle').removeClass('active');
  });
  $('.navbar-nav a').on('click', function () {
    $('.navbar-nav a').removeClass('active');
    $(this).addClass('active');
  });

  // active navbar of page current
  var urlcurrent = window.location.pathname;
  $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active');
  AOS.init({
    easing: 'ease-out-back',
    duration: 1000,
    once: true,
    offset: 30
  });
});